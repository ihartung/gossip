// app/routes.js
var fs = require("fs");
var request = require("request");
var uuid = require("node-uuid");

module.exports = function(app) {

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function(req, res) {
        if('uuid' in me) {
          res.render('profile.ejs',{
              user_name: me.user_name,
              list: messages
          });
        }
        else res.render('index.ejs'); // load the index.ejs file
    });


    app.post('/', function(req, res) {

        console.log("post/");

        var myuuid = uuid.v1();
        var fullUrl = req.protocol + '://' + req.get('host');

        console.log(req.body);

        me = { "m_count":0,"uuid": myuuid ,"url": fullUrl , "user_name" : req.body.username, "want":{} };

        console.log(JSON.stringify(me));

        res.redirect('/');

    });

    //add local message
    app.post('/local/message', function(req, res){
        //get all messages

        console.log("post/local/message");

        var messageid = me.uuid + ":" + me.m_count;
        var message = {"MessageID":messageid, "Originator": me.user_name, "Text": req.body.message}
        me.want[me.uuid] = me.m_count;
        me.m_count = me.m_count + 1;



        messages.messages[messages.count] = message;
        messages.count = messages.count + 1;


        console.log(messages);
        console.log(me.want);

        res.redirect('/');
    });




    //add message from other peer
    app.post('/message', function(req, res){
        //get all messages

        console.log("post/message");

        console.log(req.body);
        //get and parse body of request
        var message = req.body.message;

        console.log(req.body.message);

        if(message.hasOwnProperty("Want")){

          var found = false;

          for(i = 0; i < peers.peers.length; i++){
            if(peers.peers[i].url == message.Endpoint){
              peers.peers[i].want = message.Want;
              found = true;
            }
          }

          if(found==false){
            peers.peers[peers.count] = {"want": message.Want, "url":message.Endpoint};
            peers.count = peers.count + 1;
          }

          console.log(peers);

        }
        else if(message.hasOwnProperty("Rumor")){
          //Rumor

          console.log("inRumor");

          var mid = JSON.stringify(message.Rumor.MessageID).replace(/\"/g, "");
          var array = mid.split(":");
          var uuid = array[0].trim();
          var sn = array[1].trim();
          var good = false;

          console.log(uuid);
          console.log(sn);
          console.log(message.Rumor);

          //if the message uuid is in me
          if(uuid in me.want){
            //if I do not have the message
            if(sn > me.want[uuid]){
              good = true;
            }
          }
          else good = true;

          console.log(good);

          if(good){
            messages.messages[messages.count] = message.Rumor;
            messages.count = messages.count + 1;
            me.want[uuid] = sn;
          }

          console.log(messages);
          console.log(me);

        }

        res.status(200).send('OK');

    });

    //add a peer
    app.post('/peer', function(req, res){
        //var u = req.body.peer;
        console.log("post/peer");

        var url = JSON.stringify(req.body.peer).replace(/\"/g, "");
        var murl = "http://" + url;

        var peer = { "want":{}, "url" : murl}
        peers.peers[peers.count] = peer;
        peers.count = peers.count + 1;

        console.log(peers);

        res.redirect('/');

    });



};
